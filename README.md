![输入图片说明](http://git.oschina.net/uploads/images/2016/0114/215145_c8cabc17_526496.png "在这里输入图片标题")

> # `点点例`是基于[APICloud](http://www.apicloud.com/)平台开发集工具及社交类的移动应用！

* 开发作者：`新生帝`
* 作者QQ ：`8020292`
* 交流Q群 ：`18863883`  一起改变中国IT教育
* Q群描述 ：这里有一群IT技术爱好者，里面都是热爱App开发，软件开发，游戏开发，前端开发，网页设计，UI设计，软件测试，需求分析，算法分析，人工智能的小伙伴，欢迎你加入一起改变中国IT教育这个大家庭!
* 开发理念：一切从简，只为了更懒！
* 技术支持：[中山赢友网络科技有限公司](http://www.winu.net/)
* 讨论社区：[一起改变中国IT教育](http://bbs.winu.net/)
* 平台描述：[APICloud](http://www.apicloud.com/)是一个非常棒的一个Hybrid App开发平台！

## 项目状态

* `目前项目处于Alpha版本，不支持热更新，Beta版本之后才采用热更新！请知悉！`

## App测试平台

* 本人使用iPhone系列手机，小米Note，小米4等主流手机测试，均可流畅运行！如有闪退、卡顿问题，请留言并注明页面和手机型号，还有配置。

## 安装包信息

 * [0.1.6 Alpha IOS版本下载](http://downloadpkg.apicloud.com/app/download?path=http://7xpxtr.com1.z0.glb.clouddn.com/90466b6b3090403ed43150cb69e9dc05.ipa)

 * [0.1.6 Alpha Android版本下载](http://downloadpkg.apicloud.com/app/download?path=http://7xpxtr.com1.z0.glb.clouddn.com/b6ce310411244e79460010582f617a7b_d)

 * 

 * [0.1.4 Alpha IOS版本下载](http://downloadpkg.apicloud.com/app/download?path=http://7xpxtr.com1.z0.glb.clouddn.com/46fcc365a77e2d4dae492d217df8c37e.ipa)

 * [0.1.4 Alpha Android版本下载](http://downloadpkg.apicloud.com/app/download?path=http://7xpxtr.com1.z0.glb.clouddn.com/1e34cb949c75b82c955e12fa3af6406e_d)

 * 

 * [0.1.2 Alpha IOS版本下载](http://downloadpkg.apicloud.com/app/download?path=http://7xpxtr.com1.z0.glb.clouddn.com/b11ebbe9591d84148aab02a5900cc81d.ipa)

 * [0.1.2 Alpha Android版本下载](http://downloadpkg.apicloud.com/app/download?path=http://7xpxtr.com1.z0.glb.clouddn.com/6f3372ed8d34e75aa79ba01ec6d4e9c0_d)

 * 

 * [0.1.0 Alpha IOS版本下载](http://downloadpkg.apicloud.com/app/download?path=http://7xpxtr.com1.z0.glb.clouddn.com/64ecb961f4a840a6e8fbc467d50a5a18.ipa)

 * [0.1.0 Alpha Android版本下载](http://downloadpkg.apicloud.com/app/download?path=http://7xpxtr.com1.z0.glb.clouddn.com/74732d783792c054a272af5b74e06d25_d)

 * 

 * [0.0.8 Alpha IOS版本下载](http://downloadpkg.apicloud.com/app/download?path=http://7xpxtr.com1.z0.glb.clouddn.com/c6804cc784808517a1514edf50814a9a.ipa)

 * [0.0.8 Alpha Android版本下载](http://downloadpkg.apicloud.com/app/download?path=http://7xpxtr.com1.z0.glb.clouddn.com/23e10ce32977cf5aa88fec28707b3d3_d)

 * 

 * [0.0.6 Alpha IOS版本下载](http://downloadpkg.apicloud.com/app/download?path=http://7xpxtr.com1.z0.glb.clouddn.com/fd5e84e7dd524e966ac0ce039176c1dc.ipa)

 * [0.0.6 Alpha Android版本下载](http://downloadpkg.apicloud.com/app/download?path=http://7xpxtr.com1.z0.glb.clouddn.com/81397bc505fbac973702348c1c67ff35_d)

 * 

 * [0.0.4 Alpha IOS版本下载](http://downloadpkg.apicloud.com/app/download?path=http://7xpxtr.com1.z0.glb.clouddn.com/c0516f3ad8f6cc787dd9eb53b165a0e9.ipa)

 * [0.0.4 Alpha Android版本下载](http://downloadpkg.apicloud.com/app/download?path=http://7xpxtr.com1.z0.glb.clouddn.com/ec718617b626f0fa553a837db3cc0def_d)

*****

## 更新记录

```
 ### 下期版本预告
 
* [新增] 支持热更新，无需每个版本下载替换
* [新增] 常用示例：发布朋友圈页面
* [新增] 常用示例：选项卡标题更多布局
* [新增] 常用示例：常用示例：多按钮滑动切屏，类似淘宝
* [新增] 常用示例：常用示例：高度自定义日历插件
* [新增] 方寸之席：消息总览列表
* [新增] 方寸之席：用户等级列表
 
 **************************

 ### 2016年01月26日 0.1.6 Alpha 版本

* [新增] 多套侧滑主题，可以自由切换主题，下一版本更新
* [新增] 项目实战：Echart3.0柱状图十一个使用实例
* [新增] 项目实战：Echart3.0折线图十二个使用实例
* [新增] 项目实战：Echart3.0饼图五个使用实例 
* [更新] 禁止手势侧滑显示侧滑菜单，解决冲突问题
* [更新] iconfont 字体图标
* [更新] 整个侧滑设计，新增多套主题切换
* [更新] 重新调整默认导航颜色
* [更新] 流连忘返发布弹出窗
* [更新] 分享页面
* [更新] 注册身份选择页面在小屏幕的问题
* [更新] 可以直接点击侧滑菜单退出按钮退出登录
* [更新] Markdown编辑器标题样式
* [更新] 其他小更新
 
 **************************

  ### 2016年01月25日 0.1.5 Alpha 版本

* [新增] 新增db.js文件，封装db模块所有操作方法 
* [新增] 注册页面
* [新增] 注册-基本信息页面
* [新增] 注册-身份选择页面
* [更新] iconfont 字体图标
* [更新] 其他小调整
 
 **************************

 ### 2016年01月24日 0.1.4 Alpha 版本

* [新增] db模块，完成一个简单的记事本功能！可以查看列表，编辑，删除，新增等功能。
* [新增] App目录文件说明
* [新增] Markdown工具栏 强制滚动到光标区域，避免键盘盖住问题，目录工具栏，清空工具栏，星期工具栏
* [更新] iconfont 字体图标
* [更新] 重构Markdown编辑器，代码更少，渲染更快，更加强大 
* [更新] 侧滑在iPhone4上被盖住
 
 **************************

  ### 2016年01月23日 0.1.3 Alpha 版本

* [新增] Markdown工具栏 缩进功能，回到顶部功能，回到底部功能 ,向上调整光标位置，向下调整光标位置，预览功能
* [更新] iconfont 字体图标
* [更新] AHelper.js BUG，版本号为：1.1.0
* [更新] 去掉代码高亮插件，采用markdown语法渲染，更加清晰明了。 
* [更新] 重新排版 事件监听 页面
* [更新] 侧滑在iPhone4上的BUG
* [更新] 删除syntaxhighlighter代码高亮，替换成Markdown编辑器
 
 **************************

 ### 2016年01月22日 0.1.2 Alpha 版本

* [新增] Markdow语法解析插件：strapdown.js
* [新增] 常用示例：Markdown语法预览
* [新增] 常用示例：Markdown编辑器
* [新增] Markdown工具栏,方便快速插入
* [新增] Markdow编辑器换行符
* [更新] Markdown编辑器默认字体大小和边距
* [优化] Markdown编辑器渲染速度，优化了代码，更加清晰明了
* [优化] Markdown编辑器自动光标
 
 **************************

 ### 2016年01月21日 0.1.1 Alpha 版本

* [新增] 常用示例：登录页面
* [新增] 常用示例：瀑布流布局，可以放大，缩小，追加，也可以配合懒加载 ，不依赖任何框架
* [新增] 文章详情页面
* [新增] 指点迷津页面分类阴影效果
* [更新] 方寸之席 用户名处添加等级
* [更新] 侧滑背景
* [更新] 流连忘返页布局，以内容为主
* [更新] 指点迷津页面跳动情况
* [更新] 启动画为Window页
* [修复] 指点迷津页面分类下拉弹动问题
* [修复] 登录BUG，键盘弹出图片上浮BUG
 
 **************************

 ### 2016年01月20日 0.1.0 Alpha 版本

* [新增] 常用示例：九宫格、十六宫格
* [新增] 首页 活动 栏目
* [新增] Swiper.js 3.2.7版本
* [新增] 侧滑菜单背景
* [更新] 首页 图片懒加载
* [更新] 点击侧滑菜单高亮透明
* [更新] 禁止frameGroup滑动
* [更新] 葵花宝典全部样式，新增推荐列表文章 
* [更新] 指点迷津全部样式，新增推荐列表文章
* [更新] 侧滑菜单点击用户设置自动切换到指定页面
* [更新] iconfont 字体图标
* [更新] 启动页为手动移除，默认是3秒 
* [更新] 葵花宝典文案
* [修复] 指点迷津拖动页面版本也跟着下拉
* [修复] Android低版本手机 首页时间错位问题 

 **************************

 ### 2016年01月19日 0.0.9 Alpha 版本
 
* [新增] App启动画
* [新增] 清除缓存可再次打开App查看启动画 
* [新增] 系统设置选项：移动网络加载图片，默认是
* [新增] 布局列表：类似微博布局
* [新增] 布局列表：资讯列表（无图）
* [新增] 布局列表：资讯列表（有图）
* [修复] AHelper.js BUG，版本号为：1.0.8
* [更新] iconfont 字体图标
* [更新] 侧滑菜单全部样式
* [更新] 版本号数字
* [更新] 取消IOS系统侧滑缩放特效，保持和Android一样的体验
 
 **************************

 ### 2016年01月18日 0.0.8 Alpha 版本
 
* [新增] 赢友产品列表
* [新增] 下期版本预告
* [新增] 系统设置退出登录按钮
* [修复] IOS系统清除缓存错误
* [更新] 更新记录样式，取消时间轴，改为卡片模式
* [更新] 字体图标
* [更新] 常用示例：点击 + 按钮 样式
 
 **************************

 ### 2016年01月17日 0.0.7 Alpha 版本
 
* [新增] 系统设置页面
* [新增] 网络连接，未连接时监听
* [更新] 首页底部菜单名称
* [更新] 首页底部菜单：方寸之席图标
* [更新] 前端框架栏目点击选择浏览器打开相应的官网
* [更新] AUI版本为1.1.7
* [更新] Hammer.js版本为2.0.6
* [更新] 个人中心排版和内容
* [更新] 侧滑菜单流畅度
* [更新] 讨论社区未联网时提示消息
 
 **************************

 ### 2016年01月16日 0.0.6 Alpha 版本
 
 * [新增] 获取App缓存和清空缓存功能
 * [新增] App.js 全局通用文件
 * [更新] 讨论社区点赞和查看评论按钮
 * [更新] 讨论社区详细信息为动态数据
 * [更新] 讨论社区列表中内设置为只截取47个字符，点击可以查看详细内容
 * [更新] 没有网络时提示
 * [更新] 更新记录样式，使用时间轴格式
 
 **************************

 ### 2016年01月15日 0.0.5 Alpha 版本
 
 * [新增] 更新讨论社区为动态数据！调用 http://bbs.winu.net 接口
 * [新增] 讨论社区分布时间显示，模仿微信！
 
 **************************
 
 ### 2016年01月14日 0.0.4 Alpha 版本
 
* [新增] 所见所闻页添加菜单，模仿QQ控件
* [更新] 应开发者的要求，全面替换UI为AUI框架。
* [更新] 布局实例中的【向左滑出隐藏菜单】流畅度，和滑动时其他自动隐藏。
* [更新] App图标
* [更新] 大量默认文字描述
* [更新] 布局实例中的【打开普通窗口（带底部导航）】的设置页面，模仿QQ空间
 
 **************************
 
### 2016年01月13日 0.0.3 Alpha 版本
* [新增] 常见实例：前端推荐、赢友产品
* [新增] css3动画：新生帝在努力奔跑
* [新增] 常见实例：滑动页面头部透明显示、优化页面加载之图片懒加载
* [更新] 所见所闻栏目所有图片为懒加载
* [更新] 转场动画修改为新视图将旧视图推开，之前是新视图移到旧视图上面
* [修复] Zepto在IOS系统出错问题
 
 **************************
 
 ### 2016年01月12日 0.0.2 Alpha 版本
 
 * [新增] 常见实例：底部导航 +、向左滑出隐藏菜单、酷炫加载特效
 * [新增] 设备实例：完成APICloud所有端API对象的属性编写
 * [新增] 事件实例：完成APICloud所有端API对象的事件编写
 * [新增] 模块实例：预设模块功能开发功能先事项
 * [更新] 提升App点击响应速度，提高切换流畅度
 
 **************************
 
 ### 2016年01月11日 0.0.1 Alpha 版本
 
 * [新增] 常见实例：打开普通窗口、打开普通窗口（带底部导航）、打开窗口组、弹出分享窗口
 * [新增] 讨论社区浮动刷新和发布按钮
 
 **************************
 
 ### 2016年01月10日 0.0.0 Alpha 版本
 * [新增] 搭建App基本骨架
 * [新增] 常见实例、讨论社区、所见所闻、个人中心菜单
 * [新增] 侧滑菜单
 
 **************************
 
 ```

## App截图（0.0.4 Alpha版本 ）

![输入图片说明](http://git.oschina.net/uploads/images/2016/0123/162408_8b308f95_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0120/015308_fed94bcb_526496.jpeg "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0120/015256_5c3c084d_526496.jpeg "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0120/183731_69eca232_526496.jpeg "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0121/174108_1f90108f_526496.jpeg "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0121/174116_11c77255_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0121/174123_b1015463_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0121/174131_e4eb12de_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0121/174140_c90ace27_526496.jpeg "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0121/174148_2f0c4d4f_526496.jpeg "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0119/172429_176d7a96_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0119/172441_f71ae423_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0119/172449_b827f395_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0114/215340_a55b3593_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0114/215357_69ec2e65_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0114/215409_607bc830_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0114/215419_387035c5_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0114/215429_3802585e_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0114/215440_fe50c016_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0114/215451_23325a6c_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0114/215501_7a9ccd01_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0114/215510_f9d17dd3_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0114/215522_44602faf_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0114/215530_f2c3cb48_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0114/215538_07594a94_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0114/215550_bc183794_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0114/215559_483829b6_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0114/215618_beb8563c_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0114/215630_a09d8380_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0114/215639_d0da354b_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0114/215646_5449c87c_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0114/215655_7a7b73f6_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0114/215704_f0f17358_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0114/215754_95a18672_526496.png "在这里输入图片标题")